= netdiff(1) =
:doctype: manpage
/// SPDX-License-Identifier: BSD-2-Clause
/// SPDX-FileCopyrightText: Copyright Eric S. Raymond <esr@thyrsus.com>

== NAME ==
netdiff - diff across network links

== SYNOPSIS ==

*netdiff* [diff-options] [host1:]path1 [host2:]path2 

[[description]]
== DESCRIPTION ==

This tool works exactly like diff(1), except that a file or directory specification
may be preceded by a colon-delimited hostname in the style of ssh or scp.
If so, it makes a temporary copy of the file or directory locally in
order to perform the diff.

All options are simply passed to the diff command.

You will be required to authenticate to remote machines. To reduce
friction it is best to have your ssh keys installed on all machines
across which you use this tool.

[[requirements]]
== REQUIREMENTS ==

netdiff is written in pure POSIX shell. It requires a POSIX-compliant
expr(1). It uses scp(1) as a transport.

[[files]]
== FILES ==
/tmp/netdiff$$-[ab]_copy

[[bugs]]
== BUGS ==

Filenames containing "%" will confuse it.  File content on either
side of a link that matches the pattern of either of the tempfile
names will also confuse it.

There is a different "netdiff" that is a Python library for network
analysis, but the analogy of this tool with netcat(1) was impossible
to resist.

[[returns]]
== ERROR RETURNS ==

Same as diff(1); return 1 may also be caused by a network copy failure.

[[seealso]]
== SEE ALSO ==

diff(1), netcat(1), scp(1). sshexport(1).

[[author]]
== AUTHOR ==

Eric S. Raymond <esr@thyrsus.com>.
The public repository is at https://gitlab.com/esr/netdiff[on GitLab].

// end
